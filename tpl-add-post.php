<?php

/*

Template Name: Add Blog Post

*/

?>

<style>

body.page-template-tpl-add-post #main {

	float: left;

	padding: 0 30px;

	width: calc(100% - 400px);

}

.table_wrap {

	margin:0 0 30px;

}

.table_wrap table tr td, .table_wrap table tr th {

	border: 1px solid #333;

	border-collapse: collapse;

	padding: 5px;

	border-left: 0;

	border-right: 0;

}

.dsktop_hide {

	display:none;

}

.table_wrap table tr td ul {

	padding: 10px 0;

}

.table_wrap table tr td ul li {

	list-style:disc inside;

	font-size: 14px;

}

.warning_msg {

    background: #eee;

    padding: 10px;

    text-align: center;

}

.success_msg {

	background: #ec3f7f; color:#fff;

    padding: 10px;

    text-align: center;}

	

ul.wpuf-form li .wpuf-label { float:none !important; width:100% !important; margin:0 0 15px;}

ul.wpuf-form li .wpuf-fields { float:none !important; width:100% !important;}	

ul.wpuf-form li .wpuf-fields a.file-selector { background:#ec3f7f !important; border:0 !important; border-radius:0 !important; height:auto !important;}

ul.wpuf-form .wpuf-submit input[type=submit] { background:#ec3f7f !important; border:0 !important; border-radius:0 !important; }



.rt_align { text-align:right;}

.go_button {background:#ec3f7f; border:0; border-radius:0; padding:8px 15px; color:#fff; }



 @media (max-width: 479px) {

.table_wrap table tr th {

display:none;

}

.table_wrap table tr td {

display:block;

border-bottom:0;

}

}

</style>

<?php get_header(); ?>

<?php

$options = get_option('payment_gateway_settings');

@$user = wp_get_current_user();

@$user_roles = (array) $user->roles;

 ?>

<div id="content">

  <div id="inner-content" class="container p borderlr">

    <div id="sidebar1" class="sidebar">

      <?php dynamic_sidebar('sidebar left'); ?>

      <?php // Banners left side

	  if ( is_active_sidebar('Banners Left') ) : ?>

      <div class="banners-left-container">

        <?php dynamic_sidebar('Banners Left'); ?>

      </div>

      <?php endif; ?>

      

      <?php

		require_once 'Mobile_Detect.php';

		$detect = new Mobile_Detect;

		$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');

		if($deviceType=='computer'){ ?>

		

		<?php if ( is_active_sidebar( 'sidebar_banner_left_sidebar' ) ) : ?>

		<?php dynamic_sidebar( 'sidebar_banner_left_sidebar' ); ?>

		<?php endif; ?>

		

		<?php if ( is_active_sidebar( 'partnerstorecode' ) ) : ?>

		<?php dynamic_sidebar( 'partnerstorecode' ); ?>

		<?php endif; ?>

		

		<?php } ?>



    </div>

    <main id="main" role="main">

      <?php if ( !is_user_logged_in() ) { ?>

      <p class="warning_msg">Sie m&uuml;ssen eingeloggt sein um einen Blogartikel schalten zu k&ouml;nnen. <br>Bitte einloggen oder neuen Benutzeraccount erstellen. </p>

      <?php echo do_shortcode('[pie_register_login]'); ?>

      <?php }elseif(is_user_logged_in() && in_array( 'administrator',$user_roles)){?>

        <?php echo do_shortcode('[wpuf_form id="16534"]');

         echo '<div class="rt_align"><a class="go_button" href="'.get_page_link($options[post_management_page]).'">Zum Blog Artikel Management</a></div>'; ?>

      <?php }else{ ?>

      <?php             $current_user_id = get_current_user_id();

	  

	                    //two checkout start

	                     $twocheckout_secret_word = $options[twocheckout_secret_word];

						 $twocheckout_sid = $options[twocheckout_sid];

						 

						 if(!empty($_REQUEST['order_number'])&&!empty($_REQUEST['total'])&&!empty($_REQUEST['key'])){

							 

							 $hashSecretWord = $twocheckout_secret_word; //2Checkout Secret Word

							 $hashSid = $twocheckout_sid; //2Checkout account number

							 $hashTotal = $_REQUEST['total']; //Sale total to validate against 

							 $hashOrder = $_REQUEST['order_number']; //2Checkout Order Number

							 $StringToHash = strtoupper(md5($hashSecretWord . $hashSid . $hashOrder . $hashTotal));

							

							 if ($StringToHash != $_REQUEST['key']) {

								  echo $result = 'Payment Fail - Hash Mismatch';

								} else { 

								 update_user_meta($current_user_id, '_payment_status','Completed');

							     update_user_meta( $current_user_id, '_payment_completed', true );

								 $result = 'Success - Hash Matched';

							 }

						 }

						//two checkout start

	  

	  

                        $payment_completed =  get_user_meta( $current_user_id, '_payment_completed', true );

                        $payment_status =  get_user_meta( $current_user_id, '_payment_status', true );

                        

                        if(!empty($_GET['st'])){

						  if($_GET['st']==1){ $status = 'Success'; }elseif($_GET['st']==2){ $status = 'Aborted'; }else{  $status = $_GET['st'];  }

                         echo '<p class="success_msg">Thanks For Your Payment. Your Payment Is '.$status.'.</p>';

                        }

						

						if(!empty($_GET['credit_card_processed'])){

                         echo '<p class="success_msg">Thanks For Your Payment. Your Payment Is Completed.</p>';

                        }

                        

                        if($payment_completed==1){

                            

                            echo do_shortcode('[wpuf_form id="16534"]');

                            echo '<div class="rt_align"><a class="go_button" href="'.get_page_link($options[post_management_page]).'">Zum Blog Artikel Management</a></div>';

                            

                        }elseif($payment_completed!=1 && !empty($payment_status)){

                        

                          echo '<p class="success_msg">Your Payment Status Is '.$payment_status.'</p>';

                         

                        }else{

                         ?>

      <h1>Blogbeitrag zahlen</h1>

      <p>Pr&auml;sentieren Sie sich mit einem Blogbeitrag auf unserer Website und sprechen Sie eine Vielzahl an Usern an, die sich zum Thema Hochzeit informieren. Der Blogbeitrag kostet einmalig 199,00 Euro und Sie k&ouml;nnen beliebig viele Artikel ver&ouml;ffentlichen.</p>

      <div class="table_wrap">

        <table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <th>Anbieter</th>

            <th></th>

            <th>Zum Anbieter</th>

          </tr>

          <tr>

            <td><strong class="dsktop_hide">Anbieter</strong> <img src="<?php echo get_template_directory_uri().'/payment-gateway-settings/assets/img/tbl_img_1.jpg' ?>" alt="img1" /></td>

            <td><strong class="dsktop_hide"></strong>

              <ul>



              </ul></td>

            <td><strong class="dsktop_hide">Zum Anbieter</strong>

          <?php

           $amount = $options[blog_subscription_price];

           $paypal_testmode = $options[paypal_testmode];

           if($paypal_testmode==1){

              $payment_url = 'https://www.sandbox.paypal.com/webscr';

           }else{

              $payment_url = 'https://www.paypal.com/cgi-bin/webscr';

           }

           $paypal_email = $options[paypal_email];

           $title = 'Pay For Blog Post';

           

		   $payment_url_sofort = get_site_url().'/wp-content/themes/strosch/payment-gateway-settings/sofortlib/pay.php';

          ?>

              <form action="<?php echo $payment_url; ?>" method="post">

                <!-- Identify your business so that you can collect the payments. -->

                <input type="hidden" name="business" value="<?php echo $paypal_email; ?>">

                <!-- Specify a Buy Now button. -->

                <input type="hidden" name="cmd" value="_xclick">

                <!-- Specify details about the item that buyers will purchase. -->

                <input type="hidden" name="item_name" value="<?php echo $title; ?>">

                <input type="hidden" name="amount" value="<?php echo $amount; ?>">

                <input type="hidden" name="currency_code" value="EUR">

                <input type="hidden" name="custom" value="<?php echo $current_user_id; ?>">

                <input type="hidden" name="return" value="<?php echo get_page_link($options[post_article_page]); ?>">

                <input type="hidden" name="notify_url" value="<?php echo get_page_link($options[notify_url_page]); ?>">

                <input type="submit" value="Jetzt Bezahlen" />

              </form></td>

          </tr>

          <tr>

            <td><strong class="dsktop_hide">Anbieter</strong> <img src="<?php echo get_template_directory_uri().'/payment-gateway-settings/assets/img/tbl_img_2.jpg' ?>" alt="img2" /></td>

            <td><strong class="dsktop_hide"></strong>

              <ul>



              </ul></td>

            <td><strong class="dsktop_hide">Zum Anbieter</strong>

              <form action="<?php echo $payment_url_sofort; ?>"><input type="submit" value="Jetzt Bezahlen" /></form>

             </td>

          </tr>

          <tr>

            <td><strong class="dsktop_hide">Anbieter</strong> <img src="<?php echo get_template_directory_uri().'/payment-gateway-settings/assets/img/tbl_img_3.jpg' ?>" alt="img3" /></td>

            <td><strong class="dsktop_hide"></strong>

              <ul>



              </ul></td>

            <td><strong class="dsktop_hide">Zum Anbieter</strong>

            

            

           <?php

           $twocheckout_sandbox = $options[twocheckout_sandbox];

           if($twocheckout_sandbox==1){

              $twocheckout_payment_url = 'https://sandbox.2checkout.com/checkout/purchase';

           }else{

              $twocheckout_payment_url = 'https://www.2checkout.com/checkout/purchase';

           }

		   ?>

          

            <form action='<?php echo $twocheckout_payment_url; ?>' method='post'>

              <input type='hidden' name='sid' value='<?php echo $twocheckout_sid; ?>' />

              <input type='hidden' name='mode' value='2CO' />

              <input type='hidden' name='li_0_name' value='Pay For Blog Post' />

              <input type='hidden' name='li_0_price' value='<?php echo $amount; ?>' />

              <input type='hidden' name='currency_code' value='EUR' />

              <input type='hidden' name='x_receipt_link_url' value='<?php echo get_page_link($options[post_article_page]); ?>' />

              <input type="submit" value="Jetzt Bezahlen" />

            </form>

             </td>

          </tr>

        </table>

      </div>

      <?php  }?>

      <?php } ?>

    </main>

    <div id="sidebar2" class="sidebar">

      <?php dynamic_sidebar('sidebar right'); ?>

      <?php // Banners right side ?>



       <?php if($deviceType=='computer'){ ?>



		<?php if ( is_active_sidebar( 'sidebar_banner_right' ) ) : ?>

        <?php dynamic_sidebar( 'sidebar_banner_right' ); ?>

        <?php endif; ?>

        

		<?php if ( is_active_sidebar( 'partnerstorecoderight' ) ) : ?>

        <?php dynamic_sidebar( 'partnerstorecoderight' ); ?>

        <?php endif; ?>

        

        <?php } ?>



    </div>

  </div>

</div>

<?php get_footer(); ?>

