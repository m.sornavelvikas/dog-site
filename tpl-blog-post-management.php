<?php

/*

Template Name: Blog Post Management

*/

?>

<style>

body.page-template-tpl-blog-post-management #main {

	float: left;

	padding: 0 30px;

	width: calc(100% - 400px);

}

.warning_msg {

    background: #eee;

    padding: 10px;

    text-align: center;

}

.post_count {

    margin: 0 0 15px;

}

.rt_align { text-align:right;}

.go_button {background:#ec3f7f; border:0; border-radius:0; padding:8px 15px; color:#fff; }

table.items-table.post{ width:100%; }

</style>

<?php get_header(); ?>

<?php 

$options = get_option('payment_gateway_settings');

@$user = wp_get_current_user();

@$user_roles = (array) $user->roles; 

?>

<div id="content">

  <div id="inner-content" class="container p borderlr">

    <div id="sidebar1" class="sidebar">

      <?php dynamic_sidebar('sidebar left'); ?>

      <?php // Banners left side

				if ( is_active_sidebar('Banners Left') ) : ?>

      <div class="banners-left-container">

        <?php dynamic_sidebar('Banners Left'); ?>

      </div>

      <?php endif; ?>

      

       <?php

		require_once 'Mobile_Detect.php';

		$detect = new Mobile_Detect;

		$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');

		if($deviceType=='computer'){ ?>

		

		<?php if ( is_active_sidebar( 'sidebar_banner_left_sidebar' ) ) : ?>

		<?php dynamic_sidebar( 'sidebar_banner_left_sidebar' ); ?>

		<?php endif; ?>

		

		<?php if ( is_active_sidebar( 'partnerstorecode' ) ) : ?>

		<?php dynamic_sidebar( 'partnerstorecode' ); ?>

		<?php endif; ?>

		

		<?php } ?>



    </div>

    <main id="main" role="main">



		  <?php if ( !is_user_logged_in() ) { ?>

            <p class="warning_msg">Sie m&uuml;ssen angemeldet sein um Artikeln ver&ouml;ffentlichen zu k&ouml;nnen. Bitte loggen Sie sich ein oder legen Sie einen neuen Benutzer an.</p>

            <?php echo do_shortcode('[pie_register_login]'); ?>

            

         <?php }elseif(is_user_logged_in() && in_array( 'administrator',$user_roles)){?>

         

            <?php echo do_shortcode('[wpuf_dashboard]');

              echo '<div class="rt_align"><a class="go_button" href="'.get_page_link($options[post_article_page]).'">Gehen Sie zur Artikel ver&ouml;ffentlichung</a></div>'; ?>

         

         <?php }else{ ?>

             <?php 

                $current_user_id = get_current_user_id();

                $payment_completed =  get_user_meta( $current_user_id, '_payment_completed', true );

                

                if($payment_completed==1){



                    echo do_shortcode('[wpuf_dashboard]');

                    echo '<div class="rt_align"><a class="go_button" href="'.get_page_link($options[post_article_page]).'">Gehen Sie zur Artikel ver&ouml;ffentlichung</a></div>';

                    

                }else{ 

                  

                  $url = get_page_link($options[post_article_page]);

                  wp_redirect( $url );

                  exit;

                

                } ?>

         

           <?php } ?>



    </main>

    <div id="sidebar2" class="sidebar">

      <?php dynamic_sidebar('sidebar right'); ?>

      <?php // Banners right side

				if ( is_active_sidebar('Banners Right') ) : ?>

      <div class="banners-right-container">

        <?php dynamic_sidebar('Banners Right'); ?>

      </div>

      <?php endif; ?>

      

      <?php if($deviceType=='computer'){ ?>



		<?php if ( is_active_sidebar( 'sidebar_banner_right' ) ) : ?>

        <?php dynamic_sidebar( 'sidebar_banner_right' ); ?>

        <?php endif; ?>

        

		<?php if ( is_active_sidebar( 'partnerstorecoderight' ) ) : ?>

        <?php dynamic_sidebar( 'partnerstorecoderight' ); ?>

        <?php endif; ?>

        

        <?php } ?>



    </div>

  </div>

</div>

<?php get_footer(); ?>

