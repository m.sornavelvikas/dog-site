﻿</div>

<!-- end of #container -->



<footer class="footer">

  <div class="footer_top">

    <div class="container"> <img class="footer_left" src="<?php echo get_template_directory_uri(); ?>/library/images/footer_left.png" alt="Footer"/>

      <div class="footer_inner">

        <h2>Die neuesten Hochzeitstrends</h2>

        <p>Wir führen einen umfangreichen Onlineshop mit Büchern und Hochzeitsartikeln. Damit Sie keinen der neuesten Artikel verpassen, informieren wie Sie gerne mittels Newsletters.</p>

        <script type="text/javascript">







//<![CDATA[







if (typeof newsletter_check !== "function") {







window.newsletter_check = function (f) {







    var re = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-]{1,})+\.)+([a-zA-Z0-9]{2,})+$/;







    if (!re.test(f.elements["ne"].value)) {







        alert("The email is not correct");







        return false;







    }







    for (var i=1; i<20; i++) {







    if (f.elements["np" + i] && f.elements["np" + i].required && f.elements["np" + i].value == "") {







        alert("");







        return false;







    }







    }







    if (f.elements["ny"] && !f.elements["ny"].checked) {







        alert("You must accept the privacy statement");







        return false;







    }







    return true;







}







}







//]]>







</script>

        <div class="tnp tnp-subscription">

          <form method="post" action="https://hochzeit-selber-planen.com/?na=s" onsubmit="return newsletter_check(this)">

            <table cellspacing="0" cellpadding="0" border="0">

              

              <!-- email -->

              

              <tr>

                <td align="left"><input class="tnp-email" type="email" name="ne" placeholder="E-Mail Adresse" size="30" required></td>

              </tr>

              <tr>

                <td colspan="2" class="tnp-td-submit"><button type="submit">Anmelden</button></td>

              </tr>

            </table>

          </form>

        </div>
<br>

            <ul class="social">

              <li><a title="Fb" href="https://www.facebook.com/Hochzeit-selber-planen-527273813978557/" target="_blank"><i class="ion-social-facebook"></i></a></li>

              <li><a title="Google +" href="https://plus.google.com/u/5/113698618240681546185" target="_blank"><i class="ion-social-googleplus"></i></a></li>

              <li><a title="Twitter" href="https://twitter.com/Hochzeit_1" target="_blank"><i class="ion-social-twitter"></i></a></li>

              <li><a title="Pinterest" href="https://www.pinterest.de/hochzeitselber/" target="_blank"><i class="ion-social-pinterest"></i></a></li>

              <li><a title="Instagram" href="https://www.instagram.com/hochzeit_selber_planen/" target="_blank"><i class="ion-social-instagram"></i></a></li>

               <li><a title="Youtube" href="https://www.youtube.com/watch?v=6yCbHj2enh8" target="_blank"><i class="ion-social-youtube"></i></a></li>

            </ul>

        <!--<br>

        <a class="tel" href="tel:066488390988">+43 650 812 08 48</a>--> </div>

    </div>

  </div>
        
  <div class="footer_bottom">

    <div class="container">

      <?php if ( is_active_sidebar( 'footer' ) ) : ?>

      <?php dynamic_sidebar( 'footer' ); ?>

      <?php endif; ?>

    </div>

  </div>

</footer>

</div>

<?php // all js scripts are loaded in library/bones.php ?>

<?php wp_footer(); ?>



<script>







        jQuery(function() {



			jQuery('.banners-right').children('div').each(function () {



			  if ( !jQuery(this).is(':empty') ) {



			   jQuery('.banners-right').css( "border", "3px solid white" );



			  }



			});



			



			



		   jQuery('#global-search-submit').click(function(){



			    var link = jQuery("#shop-pages-dropdown-global").find(':selected').data('link');



				var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";



				var category = jQuery("#shop-pages-dropdown-global").val();



				var keyword = jQuery("#search-field-global").val();



				if(keyword != ''){



					jQuery("#global-search-submit").prop('disabled', true);



					var data = { 'action': 'advance_search_header', 'category': category, 'keyword': keyword};



					jQuery('.global-search-loder').show();



					jQuery.post(ajaxurl, data, function(response) {



						jQuery('.global-search-loder').hide();



						jQuery('#global-search-results-list').html(response);



						if(jQuery(".global-search-results").height()>500){



						 jQuery(".global-search-results").css("height","500")



						}



						jQuery('.global-search-results').show();



						jQuery("#global-search-submit").prop('disabled', false);



						



					});



				}else{

					if(link){

					  location.href = link;

					}

				

				}







			});



			

            jQuery('#search-field-global').keypress(function (e) {

               

			    var link = jQuery("#shop-pages-dropdown-global").find(':selected').data('link');

			   

                var key = e.which;

				var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";



				var category = jQuery("#shop-pages-dropdown-global").val();



				var keyword = jQuery("#search-field-global").val();

                if(key == 13){

					

				  if(keyword != ''){

					

					jQuery(".global-search-results").css("height","auto");



					jQuery("#global-search-submit").prop('disabled', true);



					var data = { 'action': 'advance_search_header', 'category': category, 'keyword': keyword};



					jQuery('.global-search-loder').show();



					jQuery.post(ajaxurl, data, function(response) {



						jQuery('.global-search-loder').hide();



						jQuery('#global-search-results-list').html(response);



						if(jQuery(".global-search-results").height()>500){



						 jQuery(".global-search-results").css("height","500")



						}



						jQuery('.global-search-results').show();



						jQuery("#global-search-submit").prop('disabled', false);



						



					});

				  



				}else{

					

					if(link){

					  location.href = link;

					}

				

				}

			  }







			});

			



			jQuery(".global-search-results").click(function(e){



				e.stopPropagation();



			});



			



			jQuery(document).click(function(){



				jQuery(".global-search-results").hide();



				jQuery(".global-search-results").css("height","auto");



			});



			

			

			jQuery(document).ready(function(){

				jQuery(".mobile_serach_open").click(function(){

					jQuery(".mobile-tab-search-container").slideToggle();

				});

			});



/*			jQuery('.banners-left-sidebar').children('div').each(function () {



			  if ( !jQuery(this).is(':empty') ) {



			   jQuery('.banners-left-sidebar').css( "border", "3px solid white" );



			  }



			});



			



		   jQuery('.partnerstorecode').children('div').each(function () {



			  if ( !jQuery(this).is(':empty') ) {



			   jQuery('.partnerstorecode').css( "border", "3px solid white" );



			  }



			});*/



			



		});







		















			//cookie consent















			















			jQuery(function() {















				var agCookie = document.cookie.replace(/(?:(?:^|.*;\s*)HochzeitNotificationOK\s*\=\s*([^;]*).*$)|^.*$/, "$1");















				if (agCookie != "yes") {















					















					jQuery('#notification').fadeIn();















					















					jQuery('#accept').click(function() {















						document.cookie = "HochzeitNotificationOK=yes; expires=Thu, 22 Dec 2050 12:00:00 GMT; path=/";















						jQuery('#notification').slideUp();















					});















				}















			});   















		















		















		</script>

<div id="notification">

  <div id="notification-inner">

    <div id="notification-text">

      <button id="accept">OK</button>

      <h4>Diese Seite setzt Cookies. </h4>

      <p> Durch die Nutzung unserer Dienste erklärt man sich damit einverstanden, daß hier Cookies gesetzt werden.  Mehr dazu im <a href="http://www.hochzeit-selber-planen.at/impressum/">Datenschutz</a></p>

    </div>

  </div>

</div>



<!-- Overlay for mobile sidebar	 -->



<div id="mobile-overlay"></div>

</body></html>