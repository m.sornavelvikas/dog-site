
jQuery(document).ready(function($){
	$( ".check-vaildate-length input" ).keyup(function() {

		if(($(this).val() != '') &&($(this).val().length < 30)){

			var count_lenght = 30 - ($(this).val().length);

			$(".check-vaildate-length-error").remove();

			$('.check-vaildate-length .wpuf-wordlimit-message').append("<span class='check-vaildate-length-error'>Bitte geben Sie noch "+ count_lenght +" Bustaben für die Überschrift</span>");

			$(this).parents("form.wpuf-form-add").find("input[type='submit']").prop( "disabled", true );

		}

		else{

			$(".check-vaildate-length-error").remove();

			$(this).parents("form.wpuf-form-add").find("input[type='submit']").prop( "disabled", false );



		}

	});

});