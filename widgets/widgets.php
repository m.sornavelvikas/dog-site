<?php

// https://www.directbasing.com/resources/wordpress/advanced-custom-fields-widget/

class Sidebar_right_tiles extends WP_Widget
{
	function Sidebar_right_tiles() 
	{
		parent::WP_Widget(false, "Sidebar Right Tiles");
	}
 
	function update($new_instance, $old_instance) 
	{  
		return $new_instance;  
	}  
 
	function form($instance)
	{  
		$title = esc_attr($instance["title"]);  
		echo "<br/>";
	}
 
	function widget($args, $instance) 
	{
		$widget_id = "widget_" . $args["widget_id"];
 
		// I like to put the HTML output for the actual widget in a seperate file
		include(realpath(dirname(__FILE__)) . "/sidebar_right_tiles.php");
	}
}
register_widget("Sidebar_right_tiles");

?>