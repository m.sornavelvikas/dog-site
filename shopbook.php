<?php
/*
 Template Name: Bookshop-Sidebar
*/
?>

<?php get_header(); ?>

			<div id="content">

				<?php // Banners right side
				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Banners Left')) : ?>
				<?php endif; ?>

				<div id="inner-content" class="wrap cf">

						<!-- Adds top block with map on home page -->
						<?php 
						//if( is_home() OR is_front_page() ) {
						// Show top block is is home and no region set
						if( get_home_url() . '/' == "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" && !getCurrentRegionName() ) {
						//if( get_home_url() . '/' == "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ) {
							wp_enqueue_script('maphighlight', get_template_directory_uri() . '/library/js/libs/maphighlight.js', array('jquery'), '', true);
							
							$recent = new WP_Query("pagename=HomeTopBlock");
							while($recent->have_posts()) {
								$recent->the_post();
							}
						    the_content();

						    ?>

						    <nav class="region-horizontal-list">
						    <?php if( function_exists('getAdRegionList') )
						    	getAdRegionList(); ?>
						    </nav>
						    
						    <?php
						}
						?>

						<div class="headline cf">
							<div class="headline-search">
								<input id="search-category-input" type="text" value="<?php if(getCurrentCategoryName()) { echo getCurrentCategoryName(); } else { _e( 'Was suchen Sie?', 'stroschtheme'); } ?>" data-empty="<?php if(getCurrentCategoryName()) { echo 'false'; } else { echo 'true'; } ?>" readonly>
								<!-- a href="<?php //echo getClearCategoryLink(); ?>" class="clear-field" id="clear-category-input"></a -->
                                <!-- CHANGES BY BERND - remove this row -->
								<?php // if( function_exists('getAdCategoryList') )
								 //	getAdCategoryList(); ?>
								 
								 <ul id="search-categories" class="search-ul w-m2" style="">
									<div class="col-12" style="float: left; margin-left: 20px;">
									<h4 style="color:#e9407d;">BELIEBTE PRODUKTE</h4>
									<?php  wp_nav_menu( array('menu' => 'BELIEBTE PRODUKTE _1' )); ?>
									</div>
									<div class="col-13" style="float: left; margin-left: 20px;">
									<h4 style="color:#e9407d;">HOCHZEITSFEIER</h4>
									<?php  wp_nav_menu( array('menu' => 'HOCHZEITSFEIER_2' )); ?>
									</div>
									
									<div class="col-13" style="float: left; margin-left: 20px;">
									<h4 style="color:#e9407d;">ZUBEHÖR</h4>
									<?php  wp_nav_menu( array('menu' => 'ZUBEHÖR_3' )); ?>
									</div>
									
									<div class="col-13" style="float: left; margin-left: 20px;">
									<h4 style="color:#e9407d;">DEKODEKORTION</h4>
									<?php  wp_nav_menu( array('menu' => 'DEKODEKORTION_4' )); ?>
									</div>
									
									<div class="col-13" style="float: left; margin-left: 20px;">
									<h4 style="color:#e9407d;">GESCHENKE / ACCESSOIRES</h4>
									<?php  wp_nav_menu( array('menu' => 'GESCHENKE _ACCESSOIRES_5' )); ?>
									</div>
									
									
									</ul>
								 
							</div>
							<div class="headline-search">
								<input id="search-region-input" type="text" value="<?php if(getCurrentRegionName()) { echo getCurrentRegionName(); } else { _e( 'Wo suchen Sie?', 'stroschtheme'); } ?>" data-empty="<?php if(getCurrentRegionName()) { echo 'false'; } else { echo 'true'; } ?>" readonly>
								<a class="clear-field" id="clear-region-input"></a>
								<?php if( function_exists('getAdRegionList') )
									getAdRegionList(); ?>
							</div>
							<a class="button" href="javascript:location.reload();"><?php _e( 'Suchen', 'stroschtheme' ); ?></a>
						</div>

						<div id="main" class="m-all t-2of3 d-5of7 cf" role="main">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<!--<header class="article-header">
									<h1 class="page-title"><?php the_title(); ?></h1>
								</header>-->

								<section class="entry-content cf" itemprop="articleBody">
									<?php
										// the content (pretty self explanatory huh)
										the_content();

										/*
										 * Link Pages is used in case you have posts that are set to break into
										 * multiple pages. You can remove this if you don't plan on doing that.
										 *
										 * Also, breaking content up into multiple pages is a horrible experience,
										 * so don't do it. While there are SOME edge cases where this is useful, it's
										 * mostly used for people to get more ad views. It's up to you but if you want
										 * to do it, you're wrong and I hate you. (Ok, I still love you but just not as much)
										 *
										 * http://gizmodo.com/5841121/google-wants-to-help-you-avoid-stupid-annoying-multiple-page-articles
										 *
										*/
										wp_link_pages( array(
											'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'bonestheme' ) . '</span>',
											'after'       => '</div>',
											'link_before' => '<span>',
											'link_after'  => '</span>',
										) );
									?>
								</section>

							</article>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page-custom.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

							<?php // Partner Store Code
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Partner Store Code')) : ?>
							<?php endif; ?>

						</div>

						<!-- Shop Links -->
						<div id="sidebar-bookshop" class="sidebar m-all t-1of3 d-2of7 last-col cf" role="complementary">

							<?php if ( is_active_sidebar( 'sidebar-bookshop' ) ) : ?>

								<?php dynamic_sidebar( 'sidebar-bookshop' ); ?>

							<?php else : ?>

								<?php
									/*
									 * This content shows up if there are no widgets defined in the backend.
									*/
								?>

								<div class="no-widgets">
									<p><?php _e( 'This is a widget ready area. Add some and they will appear here.', 'bonestheme' );  ?></p>
								</div>

							<?php endif; ?>


							<?php // Banners left side under sidebar
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Banners Left Sidebar')) : ?>
							<?php endif; ?>

						</div>


				</div>

				<?php // Banners right side
				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Banners Right')) : ?>
				<?php endif; ?>

			</div>


<?php get_footer(); ?>
