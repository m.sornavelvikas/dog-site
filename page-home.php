<?php

/*

* Template Name: Startseite

*/

?>



<?php get_header(); ?>



	<div id="content">





		<div id="inner-content" class="container p borderlr">



			<div id="sidebar1" class="sidebar">

				<?php dynamic_sidebar('sidebar left'); ?>

				<?php // Banners left side

				if ( is_active_sidebar('Banners Left') ) : ?>

					<div class="banners-left-container">

						<?php dynamic_sidebar('Banners Left'); ?>

					</div>

				<?php endif; ?>

                

               <?php

				require_once 'Mobile_Detect.php';

				$detect = new Mobile_Detect;

				$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');

				if($deviceType=='computer'){ ?>

				

				<?php if ( is_active_sidebar( 'sidebar_banner_left_sidebar' ) ) : ?>

				<?php dynamic_sidebar( 'sidebar_banner_left_sidebar' ); ?>

				<?php endif; ?>

				

				<?php if ( is_active_sidebar( 'partnerstorecode' ) ) : ?>

				<?php dynamic_sidebar( 'partnerstorecode' ); ?>

				<?php endif; ?>

				

				<?php } ?>

			</div>





			<main id="main" class="cf" role="main">



                   <?php /*if (have_posts()) : while (have_posts()) : the_post();*/ ?>



					<?php /*the_content();*/ ?>



				<?php /*endwhile; endif;*/ ?>



				<div class="hero">

					<h1>Schritt f&uuml;r Schritt zur perfekten Hochzeit</h1>

					<a href="<?php echo get_home_url(); ?>/planung-der-hochzeit-jetzt-starten/" class="button big">Hochzeitsplanung jetzt starten</a>

				</div>



	<div class="searchbox">

					<h3>Dienstleister in Ihrer N&auml;he</h3>

					



						<?php  



						if( get_home_url() . '/' == "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" && !getCurrentRegionName() ) {



							wp_enqueue_script('maphighlight', get_template_directory_uri() . '/library/js/libs/maphighlight.js', array('jquery'), '', true);

							

							$recent = new WP_Query("pagename=HomeTopBlock");



						    ?>



						    <nav class="region-horizontal-list" style="display: none;">

						    <?php

						    	getAdRegionList(3); ?>

						    </nav>

						    

						    <?php

						} else {

							getAdRegionForm();

						}



						$loc_array = getCurrentRegionArray();

						$state = $loc_array[1];

						$county = $loc_array[2];

						

						?>







					<div class="headline cf home-headline">





							<div style="float: left;
margin-right: 10px;
width: 39.5%;
position: relative;" class="headline-search">



								<input id="search-location-state-input" type="text" value="<?php if( $state ) { echo $state; } else { echo "In welchem Land?"; } ?>" data-empty="<?php if( $state ) { echo 'false'; } else { echo 'true'; } ?>" readonly>



								<a class="clear-field" id="clear-location-state-input"></a>



								<?php if( function_exists('getAdRegionStateList') )

									getAdRegionStateList(); ?>



							</div>



							<div style="float: left;
margin-right: 10px;
width: 39.5%;
position: relative;" class="headline-search">



								<input id="search-location-county-input" type="text" value="<?php if( $county ) { echo $county; } else { echo "In welchem Bundesland/Kanton?"; } ?>" data-empty="<?php if( $county ) { echo 'false'; } else { echo 'true'; } ?>" readonly>



								<a class="clear-field" id="clear-location-county-input"></a>



								<?php if( function_exists('getAdRegionCountyList') )

									getAdRegionCountyList(); ?>

									

							</div>

							<a class="button startseite" href="/hochzeitsbranchenbuch"><?php _e( 'Suchen', 'stroschtheme' ); ?></a>

						</div>







					<!-- BOTTOM STANDART FORM INPUT -->

					<?php /* <form class="inputs">



						<div class="select search-country">

							<!-- <input type="text" name="land" id="search-country" class="home-lend-input"> -->

							<select id="search-country">

								<option value="null" selected>Land</option>

								<option value="AT">&ouml;sterreich</option>

								<option value="DE">Deutschland</option>

							</select>

							<?php /* <input id="search-location-state-input" class="home-lend-input-dub" type="text" value="<?php if( $state ) { echo $state; } else { echo "In welchem Bundesland?"; } ?>" data-empty="<?php if( $state ) { echo 'false'; } else { echo 'true'; } ?>" readonly>

								<a class="clear-field" id="clear-location-state-input"></a>

								<?php if( function_exists('getAdRegionStateList') )

									getAdRegionStateList(); ?> 

						</div>





						<div class="select search-state">

							<select id="search-state">

								<option value="null" selected>Bundesland</option>

								<option value="NOE">Nieder&ouml;sterreich</option>

								<option value="W">Wien</option>

							</select>

								<!-- <input id="search-location-county-input" type="text" value="<?php //if( $county ) { echo $county; } else { echo "In welchem Bezirk?"; } ?>" data-empty="<?php //if( $county ) { echo 'false'; } else { echo 'true'; } ?>" readonly>

								<a class="clear-field" id="clear-location-county-input"></a>

								<?php //if( function_exists('getAdRegionCountyList') )

									getAdRegionCountyList(); ?> -->

						</div>

						

						<button type="submit">Suchen</button>

					</form> */ ?>



				</div>

				<div class="featured">

						

					<div style="background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/featured2.png')" alt="fetured2">

						<h2>Hochzeitsforum</h2>

						<p>Holen Sie sich Tipps, Tricks und Inspirationen f&uuml;r die Hochzeitsplanung.</p>

						<a href="/forum/" class="button small arrowright">Zum Forum</a>

					</div>

					<div style="background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/featured3.png')" alt="featured3">

						<h2>Blog</h2>

						<p>Aktuelle Beitr&auml;ge f&uuml;r eine moderne Hochzeit am Puls der Zeit.</p>

						<a href="/blog/" class="button small arrowright">Zum Blog</a>

					</div>
<div style="background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/featured1.png')" alt="featured1">

						<h2>Shop</h2>

						<p>Hier finden Sie alles was Sie f&uuml;r eine erfolgreiche Feier ben&ouml;tigen.</p>

						<a href="/shop/hochzeitsballons/" class="button small arrowright">Zum Shop</a>

					</div>

				</div>



<div class="popular_books">

					<h2><span>Beliebte B&uuml;cher</span></h2>

					<div class="slider_container">

						<div id="popular_books" class="owl-carousel">

							<?php while ( have_rows('popular_books') ) : the_row(); ?>



								<div class="item">

									<a title="<?php the_sub_field('link_title'); ?>" href="<?php the_sub_field('url'); ?>"target="_blank"><div class="img" style="background-image: url('<?php echo get_sub_field('image')['sizes']['popular-product']; ?>')"></div></a>

									<div class="text">

										<span class="h4-span-text"><?php the_sub_field('name'); ?></span>

										<img class="stars" src="<?php echo get_template_directory_uri(); ?>/library/images/stars.png" alt="stars"/>

                                        <a href="<?php the_sub_field('url'); ?>"><?php the_sub_field('details_link_text'); ?></a>

									</div>

								</div>

							<?php endwhile; ?>

						</div>

					</div>

				</div>



				<div class="popular_products">

					<h2><span>Beliebte Produkte</span></h2>

					<div class="slider_container">

						<div id="popular_products" class="owl-carousel">

							<?php while ( have_rows('popular_products') ) : the_row(); ?>

								<div class="item">

									<a title="<?php the_sub_field('link_title'); ?>" href="<?php the_sub_field('url'); ?>"target="_blank"><div class="img" style="background-image: url('<?php echo get_sub_field('image')['sizes']['popular-product']; ?>')"></div></a>

									<div class="text">

										<span class="h4-span-text"><?php the_sub_field('name'); ?></span>

										<img class="stars" src="<?php echo get_template_directory_uri(); ?>/library/images/stars.png" alt="sterne" />

                                    	<a href="<?php the_sub_field('url'); ?>"><?php the_sub_field('details_link_text'); ?></a>

									</div>

								</div>

							<?php endwhile; ?>

						</div>

					</div>

				</div>



<div class="hochzeitsmode">

					<h2><span>Hochzeitsmode</span></h2>

					<div class="slider_container">

						<div id="hochzeitsmode" class="owl-carousel">

							<?php while ( have_rows('hochzeitsmode') ) : the_row(); ?>

								<div class="item">

									<a title="<?php the_sub_field('link_title'); ?>" href="<?php the_sub_field('url'); ?>"target="_blank"><div class="img" style="background-image: url('<?php echo get_sub_field('image')['sizes']['popular-product']; ?>')"></div></a>

									<div class="text">

										<span class="h4-span-text"><?php the_sub_field('name'); ?></span>

										<img class="stars" src="<?php echo get_template_directory_uri(); ?>/library/images/stars.png" alt="sterne2"/>

                                    	<a href="<?php the_sub_field('url'); ?>"><?php the_sub_field('details_link_text'); ?></a>

									</div>

								</div>

							<?php endwhile; ?>

						</div>

					</div>

				</div>

<div class="einladungskarten">

					<h2><span>Einladungskarten</span></h2>

					<div class="slider_container">

						<div id="einladungskarten" class="owl-carousel">

							<?php while ( have_rows('einladungskarten') ) : the_row(); ?>

								<div class="item">

									<a title="<?php the_sub_field('link_title'); ?>" href="<?php the_sub_field('url'); ?>"target="_blank"><div class="img" style="background-image: url('<?php echo get_sub_field('image')['sizes']['popular-product']; ?>')"></div></a>

									<div class="text">

										<span class="h4-span-text"><?php the_sub_field('name'); ?></span>

										<img class="stars" src="<?php echo get_template_directory_uri(); ?>/library/images/stars.png" alt="sterne3"/>

                                    	<a href="<?php the_sub_field('url'); ?>"><?php the_sub_field('details_link_text'); ?></a>

									</div>

								</div>

							<?php endwhile; ?>

						</div>

					</div>

				</div>



<div class="gastebucher">

					<h2><span>G&auml;steb&uuml;cher</span></h2>

					<div class="slider_container">

						<div id="gastebucher" class="owl-carousel">

							<?php while ( have_rows('gastebucher') ) : the_row(); ?>

								<div class="item">

									<a title="<?php the_sub_field('link_title'); ?>" href="<?php the_sub_field('url'); ?>"target="_blank"><div class="img" style="background-image: url('<?php echo get_sub_field('image')['sizes']['popular-product']; ?>')"></div></a>

									<div class="text">

										<span class="h4-span-text"><?php the_sub_field('name'); ?></span>

										<img class="stars" src="<?php echo get_template_directory_uri(); ?>/library/images/stars.png" alt="sterne4" />

                                        <a href="<?php the_sub_field('url'); ?>"><?php the_sub_field('details_link_text'); ?></a>

									</div>

								</div>

							<?php endwhile; ?>

						</div>

					</div>

				</div>

<div class="hochzeitsspiele">

					<h2><span>Hochzeitsspiele</span></h2>

					<div class="slider_container">

						<div id="hochzeitsspiele" class="owl-carousel">

							<?php while ( have_rows('hochzeitsspiele') ) : the_row(); ?>

								<div class="item">

									<a title="<?php the_sub_field('link_title'); ?>" href="<?php the_sub_field('url'); ?>"target="_blank"><div class="img" style="background-image: url('<?php echo get_sub_field('image')['sizes']['popular-product']; ?>')"></div></a>

									<div class="text">

										<span class="h4-span-text"><?php the_sub_field('name'); ?></span>

										<img class="stars" src="<?php echo get_template_directory_uri(); ?>/library/images/stars.png" alt="sterne5"/>

                                    	<a href="<?php the_sub_field('url'); ?>"><?php the_sub_field('details_link_text'); ?></a>

									</div>

								</div>

							<?php endwhile; ?>

						</div>

					</div>

				</div>


               <div class="zubehor">

					<h2><span>Zubeh&ouml;r</span></h2>

					<div class="slider_container">

						<div id="zubehor" class="owl-carousel">

							<?php while ( have_rows('zubehor') ) : the_row(); ?>

								<div class="item">

									<a title="<?php the_sub_field('link_title'); ?>" href="<?php the_sub_field('url'); ?>"target="_blank"><div class="img" style="background-image: url('<?php echo get_sub_field('image')['sizes']['popular-product']; ?>')"></div></a>

									<div class="text">

										<span class="h4-span-text"><?php the_sub_field('name'); ?></span>

										<img class="stars" src="<?php echo get_template_directory_uri(); ?>/library/images/stars.png" alt="sterne6"/>

                            			<a href="<?php the_sub_field('url'); ?>"><?php the_sub_field('details_link_text'); ?></a>

									</div>

								</div>

							<?php endwhile; ?>

						</div>

					</div>

				</div>


                <img class="seperator" src="<?php echo get_template_directory_uri(); ?>/library/images/seperator.png" alt="seperator" />

				<div class="tiles">

					<div class="row _three">

						<div class="tile" style="background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/tiles/hochzeitshomepage.jpg')" alt="hochzeitshomepage">

							<h3>Hochzeitshomepage</h3>

							<a href="/hochzeitshomepage" class="button small arrowright">Zur Hochzeitshomepage</a>

						</div>

						<div class="tile" style="background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/tiles/hochzeitstisch-online.jpg')" alt="hochzeitstisch">

							<h3>Hochzeitstisch online</h3>

							<a href="/hochzeitstisch-online" class="button small arrowright">Zum Hochzeitstisch</a>

						</div>

						<div class="tile" style="background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/tiles/beliebte-apps.jpg')" alt="beliebte-apps">

							<h3>Beliebte Apps</h3>

							<a href="/apps" class="button small arrowright">Zum Apps</a>

						</div>

					</div>

					<div class="row _two">

						<div class="tile" style="background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/tiles/fotoapp.jpg')" alt="fotoapp">

							<h3>Fotoapp f&uuml;r Ihre G&auml;ste</h3>

							<a href="https://foto-app.de/" class="button small arrowright">Zur Fotoapp</a>

						</div>

						<div class="tile" style="background-image: url('<?php echo get_template_directory_uri(); ?>/library/images/tiles/gebrauchte-artikel.jpg')" alt="gebrauchte-artikel">

							<h3>Gebrauchte Artikel</h3>

							<a href="/gebrauchte-artikel" class="button small arrowright">Zum Artikeln</a>

						</div>

					</div>

				</div>






				<?php 

					// Partner Store Code

					//if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Partner Store Code')) : endif;

				?>



			</main>	



		</div>



		<?php // Banners right side

		/*if ( is_active_sidebar('Banners Right') ) :*/ ?>

			<!-- <div class="banners-right-container"> -->

				<?php // dynamic_sidebar('Banners Right'); ?>

			<!-- </div> -->

		<?php // endif; ?>



	</div>

                 <?php get_footer( 'single' ); ?>

<?php get_footer(); ?>