<?php 
function ciusan_admin__head(){
   wp_register_style('ciusan', get_template_directory_uri().'/payment-gateway-settings/assets/css/ciusan.css');
   wp_enqueue_style('ciusan');
}

function crl_admin__menu(){
   $menu_icon = get_template_directory_uri().'/payment-gateway-settings/assets/img/ciusan.png';
   add_menu_page('Payment Gateway Settings','Payment Gateway Settings','manage_options','payment-gateway-settings','payment_gateway_settings',$menu_icon,6);
}

function crl_admin_init(){
  add_action( 'admin_menu' , 'crl_admin__menu');
  add_action( 'admin_enqueue_scripts' , 'ciusan_admin__head');
}
add_action('init', 'crl_admin_init');

function payment_gateway_settings(){ 
	echo '<div class="wrap"><h2>Payment Gateway Settign</h2>';
	if (isset($_POST['save'])) {
		
		if($_POST['paypal_testmode']==1){ $paypal_testmode = 1; }else{ $paypal_testmode = 0; }
		if($_POST['twocheckout_sandbox']==1){ $twocheckout_sandbox = 1; }else{ $twocheckout_sandbox = 0; }
		
		
		
		$options['post_article_page']		        = trim($_POST['post_article_page'],'{}');
		//$options['post_edit_page']		            = trim($_POST['post_edit_page'],'{}');
		$options['post_management_page']		    = trim($_POST['post_management_page'],'{}');  
		$options['notify_url_page']		    = trim($_POST['notify_url_page'],'{}'); 
		
		$options['blog_subscription_price']		        = trim($_POST['blog_subscription_price'],'{}');
		
		//Payment Getway
		$options['paypal_email']		        = trim($_POST['paypal_email'],'{}');
		$options['paypal_testmode']		        = trim($paypal_testmode,'{}');
		
		$options['twocheckout_sid']		        = trim($_POST['twocheckout_sid'],'{}');
		$options['twocheckout_secret_word']		= trim($_POST['twocheckout_secret_word'],'{}');
		$options['twocheckout_sandbox']		    = trim($twocheckout_sandbox,'{}');
		
		$options['sofortgateway_configkey']		= trim($_POST['sofortgateway_configkey'],'{}');
		
		update_option('payment_gateway_settings', $options);
		// Show a message to say we've done something
		echo '<div class="updated ciusan-success-messages"><p><strong>'. __("Settings saved.", "Ciusan").'</strong></p></div>';
	} else {
		$options = get_option('payment_gateway_settings');
	}
	require ('admin_menu.php');
}
