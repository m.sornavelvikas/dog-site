<style>



body.single-post #main { float: left; padding: 0 30px; width: calc(100% - 400px);}

.blog_info img { display: inherit !important;}

iframe { margin:0 !important; }

.col_2 { float:left; width:50%; padding:0 10px;}



.blog_main { margin:0 0 40px;}

.blg_thmb img { width:100%; height:auto;}

.blog_main h3 { height: 22px; overflow: hidden;}

.blog_info { font-size: 1.4rem; margin:0 0 15px;}

.blog_info p { height: inherit !important; overflow: visible !important;}

ul.blog_info_top { margin: -15px 0 20px 0px;}

ul.blog_info_top li { list-style:none; display:inline-block; position:relative; margin-right:10px; font-size:11px; font-style:italic; color:#C1B8AF;}

ul.blog_info_top li:after { position:absolute; content:""; background:#C1B8AF; width:5px; height:1px; left:-9px; top:50%;}

ul.blog_info_top li:first-child:after { display:none;}





.blog_recent_main { margin:0 0 40px;}

.blg_thmb img { width:100%; height:auto;}

.blog_recent_main h3 { height: 22px; overflow: hidden;}

.blog_recent_info { font-size: 12px; margin:0 0 15px;}

.blog_recent_info p { height: inherit !important; overflow: visible !important;}

ul.blog_recent_info_top { margin: -15px 0 20px 0px;}

ul.blog_recent_info_top li { list-style:none; display:inline-block; position:relative; margin-right:10px; font-size:11px; font-style:italic; color:#C1B8AF;}

ul.blog_recent_info_top li:after { position:absolute; content:""; background:#C1B8AF; width:5px; height:1px; left:-9px; top:50%;}

ul.blog_recent_info_top li:first-child:after { display:none;}





.blg_link { text-align:right;}

.blg_link a.btn { background: #ec3f7f; display: inline-block; color: #fff; padding: 5px 10px; font-size: 12px; font-style: italic; margin-right: 10px;}

.clear { clear: both;}

.artcl_btn { margin:0 0 35px;}

.artcl_btn a.btn { display:block; text-align:center; border:1px solid #333; text-transform:uppercase; padding:10px}

.pagtn { text-align: right;}

.pagtn .wp-pagenavi a, .wp-pagenavi span { display:inline-block;}

#hidden_title { display:none !important; } 

#hidden_email { display:none !important; }

@media (max-width: 479px) {

.col_2 { width:100%;}	

}

</style>







<?php get_header(); ?>







<?php  







global $post; 







$post_admin_id = $post->post_author;







$user_info = get_userdata($post_admin_id);







$user_login = $user_info->user_login;







$date = get_the_date('F j, Y', $post->ID );







$maincategory = get_the_category($post->ID);







$maincategory_name = $maincategory[0]->cat_name;







?>







	<div id="content">







        







		<div id="inner-content" class="container p borderlr">















			<div id="sidebar1" class="sidebar">




                 <?php dynamic_sidebar( 'sidebar-blog' ); ?>


				<?php //dynamic_sidebar('sidebar left'); ?>







				<?php // Banners left side







				if ( is_active_sidebar('Banners Left') ) : ?>







					<div class="banners-left-container">







						<?php dynamic_sidebar('Banners Left'); ?>







					</div>







				<?php endif; ?>







                <?php







				require_once 'Mobile_Detect.php';







				$detect = new Mobile_Detect;







				$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');







				if($deviceType=='computer'){ ?>







				







				<?php if ( is_active_sidebar( 'sidebar_banner_left_sidebar' ) ) : ?>







				<?php dynamic_sidebar( 'sidebar_banner_left_sidebar' ); ?>







				<?php endif; ?>







				







				<?php if ( is_active_sidebar( 'partnerstorecode' ) ) : ?>







				<?php dynamic_sidebar( 'partnerstorecode' ); ?>







				<?php endif; ?>







				







				<?php } ?>







			</div>















			<main id="main" role="main">















	          <?php while (have_posts()) : the_post(); ?>







				







				<div class="main_blog">







                







                  <div class="blog_main">







                    <div class="blg_thmb"> <a href="<?php the_permalink(); ?>">







						<?php 







						if ( has_post_thumbnail() ){







						//$default_attr = array('class'=>"blog_img_".$post->ID,'alt'=>get_the_title($post->ID),'title'=>get_the_title($post->ID),);







						echo get_the_post_thumbnail($post->ID,'full'); }







						?>







                        </a>







                        <h3><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>







                      </div>







                      <div class="blog_info">







                        <ul class="blog_info_top">







                            <li>Von <?php echo $user_login; ?></li>







                            <li>in <?php echo $maincategory_name; ?></li>







                            <li><?php echo $date; ?></li>







                        </ul>







                        <?php the_content();?>







                      </div>







                   </div>







                   







                   <?php 







					$author_id = $post->post_author;







					$author_obj = get_user_by('id', $author_id);







					$author_email = $author_obj ->user_email;







					$title = get_the_title();







					?>







                    <div id="success_message_cont"></div>







                    <form class="cmxform" id="commentForm" method="get" action="">







                        






                        <input type="hidden" name="hidden_title" value="<?php echo $title;  ?>" id="hidden_title"/>






                        <input type="hidden" name="hidden_mail" value="<?php echo $author_email;  ?>" id="hidden_mail"/>







                        







                        <div class="form-group">





                        <label for="cname">Name</label>







                        <input id="cname" name="name" minlength="2" type="text" required>







                        </div>







                        <div class="form-group">







                        <label for="cemail">E-Mail</label>







                        <input id="cemail" type="email" name="email" required>







                        </div>







                        <div class="form-group">







                        <label for="phone">Telefonnummer</label>







                        <input id="phone" type="number" name="phone">







                        </div>







                         <div class="form-group comment">







                        <label for="ccomment">Kommentar (Pflichtfeld)</label>







                        <textarea id="ccomment" name="comment" required></textarea>







                        </div>







                    







                        <input class="submit" type="submit" value="Senden">







                            







                    </form>







                    







                    







                    







                    







				</div>







              <?php endwhile;  wp_reset_query(); ?>







              







			<?php







			echo '<h4>Weitere Artikel</h4>';







			echo '<ul class="owl-carousel">';







            $args = array('posts_per_page' =>10,'orderby' => 'date','post_type' => 'post','post_status' => 'publish');







            $blog_query = new WP_Query( $args );







            while ( $blog_query->have_posts() ) : $blog_query->the_post();







            $content = get_the_content($post->ID);







            $content =  strip_tags($content);







            $content_len = strlen($content);







			







			$post_admin_id = $post->post_author;







			$user_info = get_userdata($post_admin_id);







			$user_login = $user_info->user_login;







			$category = get_the_category($post->ID);







			$category_name = $category[0]->cat_name;







            ?>







           







                <li>







                <div class="col_3">







                 <div class="blog_recent_main">







                  <div class="blg_thmb"> <a href="<?php the_permalink(); ?>">







			   <?php 







				if ( has_post_thumbnail() ){







				echo get_the_post_thumbnail($post->ID,'blog-thumb'); }







				?>







					</a>







					







				  </div>







				  <div class="blog_recent_info">







                  <h3><a href="<?php the_permalink(); ?>"><?php echo get_the_title($post->ID); ?></a></h3>







                  	<ul class="blog_recent_info_top">







                    	<li>Von <?php echo $user_login; ?></li>







                        <li>in <?php echo $category_name; ?></li>







                    </ul>







					<p><?php echo substr($content,0,46);?><?php if( $content_len>46){echo '...';}?></p>







				  </div>







				         <div class="blg_link"><a class="btn" href="<?php the_permalink(); ?>">Weiter</a></div>







                         </div>







                    </div>







                 







                </li>







            







            <?php endwhile; ?>







            <?php echo '</ul>'; ?>	















			</main>















			<div id="sidebar2" class="sidebar">







				<?php dynamic_sidebar('sidebar right'); ?>







				<?php // Banners right side ?>







                <?php if($deviceType=='computer'){ ?>







                      <div class="banners-right-container">







					<?php if ( is_active_sidebar( 'sidebar_banner_right' ) ) : ?>







                    <?php dynamic_sidebar( 'sidebar_banner_right' ); ?>







                    <?php endif; ?>







                    







                  	<?php if ( is_active_sidebar( 'partnerstorecoderight' ) ) : ?>







					<?php dynamic_sidebar( 'partnerstorecoderight' ); ?>







                    <?php endif; ?>







                    







                    </div>







                    <?php } ?>







			</div>















		</div>















	</div>















<script>







jQuery(document).ready(function(){


jQuery('iframe').width('100%');





 







/////////////////







 jQuery('.owl-carousel').owlCarousel({







    loop:true,







    margin:10,







    responsiveClass:true,







    responsive:{







        0:{







            items:1,







            nav:true







        },







        600:{







            items:2,







            nav:false







        },







        1000:{







            items:3,







            nav:true,







            loop:false







        }







    }







})







/////////////////////////















jQuery("#commentForm").validate({







	rules: {







		name: "required",







		email: {







			required: true,







			email: true







		},







		phone: "required",







		comment: "required",







	},







	messages: {







		name: "Tragen Sie Ihren Name ein",







		email: "Tragen Sie Ihren E-Mail Adresse ein",







		phone: "Tragen Sie Ihre Telefonnummer ein",







		comment: "Tragen Sie Ihren Komentar ein"







	},







	submitHandler: function() {







     







		var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";







		var cname = jQuery("#cname").val();







		var cemail = jQuery("#cemail").val();







		var phone = jQuery("#phone").val();







		var ccomment = jQuery("#ccomment").val();







		







		var hidden_title = jQuery("#hidden_title").val();







		var hidden_mail = jQuery("#hidden_mail").val();







		







		var data = { 'action': 'blog_mail_send_action', 'cname': cname, 'cemail': cemail, 'phone': phone, 'ccomment': ccomment, 'hidden_title': hidden_title, 'hidden_mail': hidden_mail };







		jQuery.post(ajaxurl, data, function(response) {







			if(response == 'Success'){







			 jQuery('#commentForm')[0].reset();;







			 jQuery( "#success_message_cont" ).html("<p id='success_message'>Vielen Dank f�r Ihr Komentar.</p>");







			}







			







		});







	







   }







	







}); 







 







});







</script>







<?php get_footer(); ?>















