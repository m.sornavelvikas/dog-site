<?php
/*
Template Name: Product Details
*/
die();
?>
<style>
body.page-template-tpl-product-detail #main {
	float: left;
	padding: 0 30px;
	width: calc(100% - 220px);
}
.col_2 {
	float:left;
	width:50%;
	padding:0 10px;
}
.clear {
	clear: both;
}

.detail_top {border-bottom: 1px solid #dfe0e2; margin:0 0 30px; padding:0 0 30px;}
.detail_image { margin:0 0 25px;}
.detail_thumb ul { margin:0; padding:0; list-style:none;}
.detail_thumb ul li { list-style:none; display:inline-block; vertical-align:top; width:110px;}

.dtls_rt h1 { text-transform:uppercase; font-size:22px; color:#323b47;}
.price-container { margin:0 0 30px;}
.price-container .price { float:left; margin-right:10px; font-size:40px; line-height: 40px; color:#323b47;}
.price-container .additional-data { float:left;}
.price-container .additional-data span { display:block;}
.price-container .additional-data span.available { text-transform:uppercase; color:forestgreen;}
.price-container .additional-data span.tax-details a { text-decoration:underline; color:#323b47;}
.price-container .additional-data span.tax-details a:hover { color:#ec3f7f;}

.buy-container { border-bottom: 1px solid #dfe0e2; margin:0 0 20px; padding:0 0 20px; }
.buy-container .amount-container { float:left; margin-right: 15px;}
.buy-container .amount-container label { display:block; margin:0 0 4px; text-align:center;}
.buy-container .amount-container .amount-down, .buy-container .amount-container .amount-up { color: #ec3f7f; font-size: 1.125em; cursor: pointer;}
.buy-container .amount-container input[type=text] { width: 36px; text-align: center; font-size: 1.125em; border: 1px solid #dfe0e2; padding-top: 4px;  padding-bottom: 4px;}
.buy-container a.btn { background:#ec3f7f; color:#fff; padding: 12px 40px;  display: inline-block; float: left; margin-top: 10px;}

.related_prod ul { margin:0; padding:0; list-style:none;}
.related_prod ul li { list-style:none; display:inline-block; vertical-align:top; width:32%;}
.rltd_box { text-align:center; padding: 0 10px;}
.rltd_box .imgbox img { width:100%;}
.rltd_box h3 { font-size: 14px; height: 40px; overflow: hidden;}
.rltd_box a.btn {display: block; background: #ec3f7f;  border-radius: 0 2px 2px 0; color: #fff; font-size: 18px; position:relative;text-align: center;}
.rltd_box a.btn span.bubble { background: url(/wp-content/themes/strosch/images/heart.png) alt="herz9" #ec3f7f no-repeat center center; border-radius: 45px; display: block; height: 45px; left: -7px; position: absolute; top: -10px; width: 45px; z-index: 1; padding: 4px 5px; font-size: 28px;}




@media (max-width: 767px) {
.detail_top .col_2 { width: 100%;}


}
@media (max-width: 479px) {
.detail_top { text-align:center;}
.price-container .price, .price-container .additional-data { float: none;}
.price-container .price { margin:0 0 10px;}
.buy-container .amount-container { float: none;  margin-right: 0;}
.buy-container a.btn { float:none;}


}
</style>
<?php get_header(); ?>
<?php
function aws_signed_request($region, $params, $version='2011-08-01')
{
   
    // Amazon de
    $region = 'de';
	$method = 'GET';
    $host = 'webservices.amazon.'.$region;
    $uri = '/onca/xml';

	//  origin amazon product api credentials

	$public_key = 'AKIAIBQSDIXISC4GIBIQ';
	$private_key = 'aB/21FthqYG4W4e3FOd9sCBP/ZhOHfERyA3Q69xj';
	$associate_tag = 'wwwhochzeitse-21';
    
   
    $params['Service'] = 'AWSECommerceService';
    $params['AWSAccessKeyId'] = $public_key;
   
    $params['Timestamp'] = gmdate('Y-m-d\TH:i:s\Z');
   
    $params['Version'] = $version;
    if ($associate_tag !== NULL) {
        $params['AssociateTag'] = $associate_tag;
    }
    
  
    ksort($params);
    
  
    $canonicalized_query = array();
    foreach ($params as $param=>$value)
    {
        $param = str_replace('%7E', '~', rawurlencode($param));
        $value = str_replace('%7E', '~', rawurlencode($value));
        $canonicalized_query[] = $param.'='.$value;
    }
    $canonicalized_query = implode('&', $canonicalized_query);
    
    
    $string_to_sign = $method."\n".$host."\n".$uri."\n".$canonicalized_query;
    
   
    $signature = base64_encode(hash_hmac('sha256', $string_to_sign, $private_key, TRUE));
    
   
    $signature = str_replace('%7E', '~', rawurlencode($signature));
    
  
    $request = 'http://'.$host.$uri.'?'.$canonicalized_query.'&Signature='.$signature;
    
    return $request;
}
?>
<div id="content">
  <div id="inner-content" class="container p borderlr">
    <?php /*?><div id="sidebar1" class="sidebar">
      <?php dynamic_sidebar('sidebar left'); ?>
      <?php // Banners left side

	if ( is_active_sidebar('Banners Left') ) : ?>
      <div class="banners-left-container">
        <?php dynamic_sidebar('Banners Left'); ?>
      </div>
      <?php endif; ?>
      <?php
	 require_once 'Mobile_Detect.php';
	 $detect = new Mobile_Detect;
	 $deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
	 if($deviceType=='computer'){ ?>
      <?php if ( is_active_sidebar( 'sidebar_banner_left_sidebar' ) ) : ?>
      <?php dynamic_sidebar( 'sidebar_banner_left_sidebar' ); ?>
      <?php endif; ?>
      <?php if ( is_active_sidebar( 'partnerstorecode' ) ) : ?>
      <?php dynamic_sidebar( 'partnerstorecode' ); ?>
      <?php endif; ?>
      <?php } ?>
    </div><?php */?>
    <main id="main" role="main">
		<div class="detail_top">
        	<div class="col_2">
            	<div class="detail_image">
                	<img src="https://ja-hochzeitsshop.de/media/catalog/product/cache/1/image/445x/9df78eab33525d08d6e5fb8d27136e95/t/o/toilettenpapier_hochzeit.jpg" alt="toilettenpapier" />
                </div>
                <div class="detail_thumb">
                	<ul>
                    	<li><a href="#"><img src="https://ja-hochzeitsshop.de/media/catalog/product/cache/1/image/445x/9df78eab33525d08d6e5fb8d27136e95/t/o/toilettenpapier_hochzeit.jpg" alt="toilettenpapier2" /></a></li>
                    </ul>
                </div>
            </div>
            <div class="col_2">
            	<div class="dtls_rt">
                	<h1>Toilettenpapier "Just Married" </h1>
                    <div class="price-container">
                    	<div class="price">3,90</div>
                        <div class="additional-data">
                        	<span class="available">Auf Lager</span>
                            <span class="tax-details">(inkl. 19% MwSt., zzgl. <a href="#">Versandkosten</a>)</span>
                        </div>
                     <div class="clear"></div>
                    </div>
                    
                    <div class="buy-container">
                    	<div class="amount-container">
                        	<label for="qty">Menge:</label>
                            <span class="amount-down">-</span>
                            <input type="text" name="qty" id="qty" maxlength="12" value="1" title="Menge" class="input-text qty">
                            <span class="amount-up">+</span>
                        </div>
                        <a href="#" class="btn">In den Warenkorb</a>
                    <div class="clear"></div>
                    </div>
                    
                    <div class="details_content">
                    	<p>Bei den Gästen hebt dieses Accessoire garantiert die Stimmung! Verbreitet auf Eurer Feier auch am „stillen Örtchen“ eine Spur Hochzeitsatmosphäre…</p>
                        <p>Dieses „Just Married“-WC-Papier ist ein ganz besonderes Highlight und nicht auf jeder Hochzeit zu sehen!</p>
                        <p>Inhalt: 3-lagig, 200 Blatt</p>
                    </div>
                    
                </div>
            </div>
        <div class="clear"></div>
        </div>
        
        <div class="related_prod">
        	<ul>
            	<li>
                	<div class="rltd_box">
                    	<div class="imgbox"><a href="#"><img src="https://ja-hochzeitsshop.de/media/catalog/product/cache/1/small_image/232x/9df78eab33525d08d6e5fb8d27136e95/2/0/201_0.jpg" alt="small" /></a></div>
                        <h3><a href="#">Seifenblasen Weddingbubbles "Ice Cream" (1 Stück) - mint</a></h3>
                        <a href="#" class="btn"> Ja, ich will <span class="bubble"></span></a>
                    </div>
                </li>
                <li>
                	<div class="rltd_box">
                    	<div class="imgbox"><a href="#"><img src="https://ja-hochzeitsshop.de/media/catalog/product/cache/1/small_image/232x/9df78eab33525d08d6e5fb8d27136e95/2/0/201_0.jpg" alt="ja-hochzeit" /></a></div>
                        <h3><a href="#">Seifenblasen Weddingbubbles "Ice Cream" (1 Stück) - mint</a></h3>
                        <a href="#" class="btn"> Ja, ich will <span class="bubble"></span></a>
                    </div>
                </li>
                <li>
                	<div class="rltd_box">
                    	<div class="imgbox"><a href="#"><img src="https://ja-hochzeitsshop.de/media/catalog/product/cache/1/small_image/232x/9df78eab33525d08d6e5fb8d27136e95/2/0/201_0.jpg" alt="small3" /></a></div>
                        <h3><a href="#">Seifenblasen Weddingbubbles "Ice Cream" (1 Stück) - mint</a></h3>
                        <a href="#" class="btn"> Ja, ich will <span class="bubble"></span></a>
                    </div>
                </li>
            </ul>
        <div class="clear"></div>	
        </div>
        
    </main>
    <div id="sidebar2" class="sidebar">
      <?php dynamic_sidebar('sidebar right'); ?>
      <?php // Banners right side
	  if ( is_active_sidebar('Banners Right') ) : ?>
      <div class="banners-right-container">
        <?php dynamic_sidebar('Banners Right'); ?>
      </div>
      <?php endif; ?>
      <?php if($deviceType=='computer'){ ?>
      <?php if ( is_active_sidebar( 'sidebar_banner_right' ) ) : ?>
      <?php dynamic_sidebar( 'sidebar_banner_right' ); ?>
      <?php endif; ?>
      <?php if ( is_active_sidebar( 'partnerstorecoderight' ) ) : ?>
      <?php dynamic_sidebar( 'partnerstorecoderight' ); ?>
      <?php endif; ?>
      <?php } ?>
    </div>
  </div>
</div>
<?php get_footer(); ?>
